---
title: Consistent Logging In Python
subtitle: dictConfig is your friend
date: 2017-11-12
tags: ["logging", "python"]
categories: ["Python"]
---


Logging is a typical kind of question but is often just given a cursory look. There are plenty of articles titled 
something like, 'All you ever need to know about logging in Python' that essentially reword python docs. Frequently, 
one of my favorite logging tools, the dictConfig, gets left out of the article entirely. Let's remedy that.

First, when you have a question about a basic tool, remember that Python comes with 'batteries included' and does a 
decent job of providing information.

* [Python 2 API](https://docs.python.org/2/library/logging.html)
* [Python 2 Tutorial](https://docs.python.org/3/howto/logging.html#logging-basic-tutorial)
* [Python 3 API](https://docs.python.org/3/library/logging.html)
* [Python 3 Tutorial](https://docs.python.org/3/howto/logging.html#logging-basic-tutorial)

Those documents are the basics and generally accessible to the novice. Let's just summarize and move on.

Loggers:
- can be named and retrieved anywhere in an interpreter instance
- have handlers that handle a log in a specialized way 
- loggers can be configured by file

Here's the extra bit I want to talk about - these loggers don't have to be setup by hand each time. Given a dictionary 
of logging parameters, Python's logging module can configure itself. There are a few nice ways to store dictionaries and
lists in plain text including YAML and JSON (both formats have corresponding libraries for reading and writing in 
Python). It doesn't matter what you use to store your configuration as long as it ends up in a readable form for 
dictConfig(). I'm partial to YAML so let's use that.

Here is an example log configuration file in YAML. It should be relatively straight forward to read and conrresponds to 
the dictConfig() facility.

###Example: Setting Up the Log Configuration

``` YAML
version: 1
disable_existing_loggers: False
formatters:
    simple:
        datefmt: '%m/%d/%Y %I:%M:%S %p'
        format: '%(asctime)s, %(levelname)s, %(message)s'

    detailed:
        datefmt: '%m/%d/%Y %I:%M:%S %p'
        format: '%(asctime)s, %(levelname)s, %(module)s::%(lineno)s, %(message)s'
        
handlers:
    console:
        class: logging.StreamHandler
        level: INFO
        formatter: simple
        stream: ext://sys.stdout

    file_handler:
        class: logging.handlers.RotatingFileHandler
        level: INFO
        formatter: simple
        filename: log.log
        maxBytes: 10485760 # 10MB
        backupCount: 20
        encoding: utf8

    error_file_handler:
        class: logging.handlers.RotatingFileHandler
        level: ERROR
        formatter: detailed
        filename: error.log
        maxBytes: 10485760 # 10MB
        backupCount: 20
        encoding: utf8

loggers:
    my_module:
        level: INFO
        handlers: [console]
        propagate: no

root:
    level: ERROR
    handlers: [console, file_handler, error_file_handler]
```


Python has quite a few handlers but console and file handling are by far the most typical. In another post, we'll use 
the stream handler and email handler to show examples of forwarding logs on to people who may need to read them ASAP.
In this example, I configured a root logger as well as a named logger. Also notice the two different file handlers have
different log formats. The error log provides much more detailed information about the log because it references the detailed formatter while the 'standard log' references the simple formatter.

###Example: Initializing and Using the Log

The next step is to use this in actual code. Here we create the entry script for your code, main.py as well as a basic 
module, mymodule.py that will be called from main.py. An important thing to remember is you should initialize this in
the main scope.

```python

"""
main.py: python script acting as your entry point
"""

import os
import yaml
import logging.config
import mymodule 

def init_log(log_config_file = None):
    # try the argument, then  the environment, then a default
    if log_config_file is None:
        log_config_file = os.getenv('LOG_CONFIG_FILE', 'log_config.yml')
    # first try to load file, then bail out to defaults

    if os.path.exists(log_config_file):
        with open(log_config_file) as f:
            config = yaml.safe_load(f)
        # you can modify your config here if you wanted:
        # config['handlers']['file_handler']['filename'] = 'mymodule.log'
        logging.config.dictConfig(config)
    else:
        # Set your preferred defaults here ...
        logging.basicConfig(level=logging.DEBUG)
        logging.warning('could not find {log_config_file}.  Using default configuration.')

def main():
    init_log()
    logging.info('logging from main')
    mymodule.my_func()

if __name__ == '__main__':
    main()
```

```python
"""
mymodule.py: An example module file to show the log configuration propating
"""

import logging

def my_func():
    logging.info('logging from my_func')

```
    
###Example: Running the code
Now if we run python main.py, we see the output on screen
<pre>
λ python main.py
05/27/2017 07:10:26 PM, INFO, main::24, Logging from main
05/27/2017 07:10:26 PM, INFO, mymodule::4, logging from my_func
</pre>

and get two log files, log.log and errors.log


###Conclusion

We did all of that that so we can both get a consistent and ubiquitous logging style across our packages. For my work in
general, each package I write references a cloud configuration file and caches it locally. This allows you to change 
your logging handlers without having to change your previous packages configuration indepdently or reinstall packages 
that were already installed.

I hope this helps give some perspective on generating logs that are easy to search, sort and illustrate a simple way to 
keep your logging consistent throughout your project. There are many more facilities available for logging include 
thread and multiprocess logging, LogRecords, more handlers, named loggers and so on. Have a look at the links provided 
earlier for more cookbook style examples and tutorials.